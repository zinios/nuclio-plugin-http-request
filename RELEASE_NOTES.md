Release Notes
-------------
1.1.1
-----
* Fixed issue with getPort().

1.1.0
-----
* Added a new method "getHostname". This will return ONLY the hostname, without any port information.
* Added a new method "getPort". This will return ONLY the port of the requested hostname.

1.0.0
-----
* Initial Release.
