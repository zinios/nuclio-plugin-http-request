<?hh //partial
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\request
{
	function getMappedGlobal(string $type):Map<string,string>
	{
		switch ($type)
		{
			case 'SERVER':	return new Map($_SERVER);	break;
			case 'GET':		return new Map($_GET);		break;
			case 'POST':	return new Map($_POST);		break;
			case 'REQUEST':	return new Map($_REQUEST);	break;
			case 'COOKIE':	return new Map($_COOKIE);	break;
			case 'FILES':	return new Map($_FILES);	break;
			default:
			{
				throw new RequestException(sprintf('Invalid global "%s"',$type));
			}
		}
		
	}
}
