<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\request
{
	require_once(__DIR__.'/types.hh');
	require_once(__DIR__.'/globals.hh');
	
	use nuclio\core\
	{
		plugin\Plugin,
		ClassManager
	};
	
	use nuclio\plugin\format\
	{
		reader\Reader,
		driver\json\JSON
	};
	
	/**
	 * 
	 * @package nushell\core
	 */
	<<singleton>>
	class Request extends Plugin
	{
		private HTTP_MAP $server;
		private HTTP_MAP $query;
		private HTTP_MAP $post;
		private HTTP_MAP $request;
		private HTTP_MAP $cookie;
		private HTTP_MAP $files;
		private HTTP_RAW $raw;
		private ?JSON $json;
		
		private HTTP_MAP $put;
		private HTTP_MAP $head;
		private HTTP_MAP $options;
		
		private HTTP_MAP $headers;
		private Vector<string> $languages;
		private Vector<string> $responseFormats;
		
		private ?HTTP_METHOD $method=null;
		
		public static function getInstance(/* HH_FIXME[4033] */...$args):Request
		{
			$instance=ClassManager::getClassInstance(self::class);
			return ($instance instanceof self)?$instance:new self();
		}
		
		/**
		 * Sets up the class by gathering the HTTP globals into Maps.
		 */
		public function __construct()
		{
			parent::__construct();
			
			//Fetch data
			$server					=getMappedGlobal('SERVER');
			$query					=getMappedGlobal('GET');
			$post					=getMappedGlobal('POST');
			$request				=getMappedGlobal('REQUEST');
			$cookie					=getMappedGlobal('COOKIE');
			$files 					=getMappedGlobal('FILES');
			
			//Null check the data and create empty objects for any nulls.
			$this->server			=!is_null($server)	?$server:Map{};
			$this->query			=!is_null($query)	?$query:Map{};
			$this->post				=!is_null($post)	?$post:Map{};
			$this->request			=!is_null($request)	?$request:Map{};
			$this->cookie			=!is_null($cookie)	?$cookie:Map{};
			$this->files			=!is_null($files)	?$files:Map{};
			$this->raw				=file_get_contents('php://input');
			$this->json				=Map{};
			
			$this->put				=Map{};
			$this->head				=Map{};
			$this->options			=Map{};
			
			$this->headers			=$this->mapHeaders();
			$this->languages		=$this->parseAcceptedLanguages($this->getHeader('accept-language'));
			$this->responseFormats	=$this->parseAcceptedResponseFormats($this->getHeader('accept'));
			
			if ($this->getHeader('content-type')=='application/json')
			{
				$JSON=JSON::getInstance();
				$this->json=$JSON->decode($this->raw);
			}
			
			if ($server->containsKey('REQUEST_METHOD'))
			{
				$this->method=$this->parseMethod($server['REQUEST_METHOD']);
			}
			
		}
		
		private function mapHeaders():HTTP_MAP
		{
			$headers=Map{};
			foreach ($this->server as $key=>$val)
			{
				$split=new Vector(explode('HTTP_',$key,2));
				if ($split->containsKey(1))
				{
					$header=str_replace('_','-',strtolower($split->get(1)));
					$headers[$header]=$val;
				}
			}
			return $headers;
		}
		
		private function parseAcceptedLanguages(?string $languageHeader):Vector<string>
		{
			if (!is_null($languageHeader))
			{
				//Remove junk.
				$parts=explode(';',$languageHeader,2);
				//Return formatted langs.
				return new Vector(explode(',',$parts[0]));
			}
			return Vector{};
		}
		
		private function parseAcceptedResponseFormats(?string $acceptHeader):Vector<string>
		{
			if (!is_null($acceptHeader))
			{
				//Remove junk.
				$parts=explode(';',$acceptHeader,2);
				//Return formatted response formats.
				return new Vector(explode(',',$parts[0]));
			}
			return Vector{};
		}
		
		private function parseMethod(mixed $method):HTTP_METHOD
		{
			if (!is_string($method))
			{
				return HTTP_METHOD::GET;
			}
			switch ($method)
			{
				case 'GET':		return HTTP_METHOD::GET;
				case 'POST':	return HTTP_METHOD::POST;
				case 'PUT':		return HTTP_METHOD::PUT;
				case 'DELETE':	return HTTP_METHOD::DELETE;
				// case 'TRACE':	return HTTP_METHOD::TRACE;
				// case 'CONNECT':	return HTTP_METHOD::CONNECT;
				case 'HEAD':	return HTTP_METHOD::HEAD;
				case 'OPTIONS':	return HTTP_METHOD::OPTIONS;
				default:		return null;
			}
		}
		
		public function isMethod(string $method):bool
		{
			return $method==$method=$this->getMethod();
		}
		
		public function getMethod():?HTTP_METHOD
		{
			return $this->method;
		}
		
		public function getQuery(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->query;
			}
			else
			{
				return $this->query->get($key);
			}
		}
		
		public function getPost(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->post;
			}
			else
			{
				return $this->post->get($key);
			}
		}
		
		public function getPut(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->put;
			}
			else
			{
				return $this->put->get($key);
			}
		}
		
		public function getHead(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->head;
			}
			else
			{
				return $this->head->get($key);
			}
		}
		
		public function getOption(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->options;
			}
			else
			{
				return $this->options->get($key);
			}
		}
		
		public function getServer(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->server;
			}
			else
			{
				return $this->server->get($key);
			}
		}
		
		public function getCookie(?string $key=null):mixed
		{
			if (is_null($key))
			{
				return $this->cookie;
			}
			else
			{
				return $this->cookie->get($key);
			}
		}
		
		public function getFile(?string $key=null):mixed
		{
			if(is_null($key))
			{
				return $this->files;
			}
			else
			{
				return $this->files->get($key);
			}
		}

		public function getRequest():mixed
		{
			return $this->request;
		}
		
		public function getRaw():mixed
		{
			return $this->raw;
		}
		
		public function getJSON():?HTTP_MAP
		{
			return $this->json;
		}

		public function getHeaders():HTTP_MAP
		{
			return $this->headers;
		}
		
		public function getHeader(string $header):?string
		{
			return $this->headers->get($header);
		}
		
		public function getLanguages():Vector<string>
		{
			return $this->languages;
		}
		
		public function getResponseFormats():Vector<string>
		{
			return $this->responseFormats;
		}
		
		public function isSecure():bool
		{
			//HHVM provides this very handy server param.
			return $this->server->get('HTTPS')=='on';
		}
		
		public function isAjax():bool
		{
			return $this->getHeader('X-Requested-With')=='XMLHttpRequest';
		}
		
		public function getDomain():?string
		{
			return $this->getServer('SERVER_NAME');
		}
		
		public function getHost():?string
		{
			return $this->getServer('HTTP_HOST');
		}

		public function getHostname():?string
		{
			list($hostname)=explode(':',$this->getServer('HTTP_HOST'));
			return $hostname;
		}

		public function getPort():?int
		{
			list($_,$port)=explode(':',$this->getServer('HTTP_HOST'));
			return (int)$port;
		}
	}
}
