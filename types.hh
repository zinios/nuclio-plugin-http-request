<?hh //strict
/*****************************************************************************
 *                                                                           *
 * This file is part of the Nuclio framework.                                *
 *                                                                           *
 * (c) Zinios <support@zinios.com>                                           *
 *                                                                           *
 * For the full copyright and license information, please view the LICENSE   *
 * file that was distributed with this source code.                          *
 *                                                                           *
 *****************************************************************************/
namespace nuclio\plugin\http\request
{
	type HTTP_MAP	=Map<string,string>;
	type HTTP_RAW	=string;
	
	enum HTTP_METHOD:string
	{
		GET		='GET';
		POST	='POST';
		PUT		='PUT';
		DELETE	='DELETE';
		// TRACE	='TRACE';
		// CONNECT	='CONNECT';
		HEAD	='HEAD';
		OPTIONS	='OPTIONS';
	}
}